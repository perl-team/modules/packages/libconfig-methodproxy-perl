Source: libconfig-methodproxy-perl
Section: perl
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Peter Pentchev <roam@debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libmodule-runtime-perl,
 libnamespace-clean-perl,
 libstrictures-perl,
 libtest-fatal-perl,
 libtest2-suite-perl,
 perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libconfig-methodproxy-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libconfig-methodproxy-perl.git
Homepage: https://metacpan.org/release/Config-MethodProxy
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no

Package: libconfig-methodproxy-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends},
 libmodule-runtime-perl,
 libnamespace-clean-perl,
 libstrictures-perl
Description: Perl module for specifying method calls in a static configuration
 The method proxy concept provided by the Config::MethodProxy module is
 a particular data structure which, when found, is replaced by the value
 returned by calling that method.  In this way static configuration can be
 setup to call your code and return dynamic contents.  This makes static
 configuration much more powerful, and provides the ability to be more
 declarative in how dynamic values make it into the configuration.
